<?php

//Include rules integration
include_once './' . drupal_get_path('module', 'pincode_login') . '/includes/pincode_login_rules.inc';

/**
 * Implementation of hook_menu().
 */
function pincode_login_menu() {
  $items = array();
    $items['pincode/login'] = array(
      'title' => 'Login',
      'description' => 'Login',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('pincode_login_pincode_form'),
      'access arguments' => array('use pincode login'),
    );
    return $items;
}

/**
 * Implementation of hook_perm().
 */
function pincode_login_perm() {
  return array('administer pincode login', 'use pincode login');
}

/**
 * Form definition for the pincode login form
 */
function pincode_login_pincode_form() {
  $form = array();  
  if ( _pincode_login_is_allowed_for_ip($_SERVER['REMOTE_ADDR'])) {
    $form = _pincode_login_get_pincodeform();
  }
  else {
    $fail = _pincode_login_get_previous_fail_for_ip($_SERVER['REMOTE_ADDR']);
    $minutes = $fail->time_until_next_allowed_attempt();
    drupal_set_message(t('You have exceeded the allowed number of login attempts. <br /> Please try again in %time', 
                        array('%time' => format_plural($minutes, '1 minute', '@count minutes'))));
  }
  
  return $form;
}

function _pincode_login_get_pincodeform() {
  $form = array();
  
  $form['pincode'] = array(
    '#type' => 'textfield',
    '#maxlength' => 4,
    '#size' => 4 ,
    '#required' => TRUE
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Log in')
  );
  return $form;
}

/**
 * Validation of pincode login form
 * On success the user is logged in
 * after validation 
 */  
function pincode_login_pincode_form_validate($form, &$form_state) {
  if (_pincode_login_dologin($form_state['values']['pincode'])) {
    _pincode_reset_failed_logins($_SERVER['REMOTE_ADDR']);
  } 
  else {
    _pincode_login_failed_login($_SERVER['REMOTE_ADDR']);
    form_set_error('pincode', t('The pincode you entered was not a valid pincode'));
  } 
}


/**
 * Orchestrates registration or updating of
 * records of failed logins
 */    
function _pincode_login_failed_login($ip) {
  if (!_pincode_login_ip_have_failed_before($ip)) {
    _pincode_login_register_first_fail_for_ip($ip);
  }
  else {
    _pincode_login_increase_failed_login(_pincode_login_get_previous_fail_for_ip($ip));
  }
}

/**
 *  Checks if a failed attempt is allready registered
 *   
 * @param
 *    string, Ip adress of client
 *      
 * @return
 *    boolean
 */
function _pincode_login_ip_have_failed_before($ip) {
  return db_result(db_query("SELECT * FROM {pincode_login} WHERE IP = '%s'", $ip)) > 0;
}

/**
 * Register a new record for failed login on given IP adress
 * 
 * @param
 *    string, Ip adress of client
 * 
 */
function _pincode_login_register_first_fail_for_ip($ip) {
  include_once './' . drupal_get_path('module', 'pincode_login') . '/includes/pincode_login_fail.inc';
  $fail = new pincode_login_fail($ip);
  drupal_write_record('pincode_login', $fail); 
}

/**
 * Updates an excisting record of field login
 * 
 * @param
 *    pincode_login_fail object
 * 
 */
function _pincode_login_increase_failed_login($fail) {
  $fail->count++;
  $fail->time = time();
  drupal_write_record('pincode_login', $fail, 'ip');
}

/**
 * Gets a data object for registered fails on given IP
 * 
 * @param
 *    string, Ip adress of client
 *    
 * @return
 *    pincode_login_fail object, Populated fail object from Db
 *  
 */        
function _pincode_login_get_previous_fail_for_ip($ip) {
  include_once './' . drupal_get_path('module', 'pincode_login') . '/includes/pincode_login_fail.inc';
  $raw = db_fetch_object(db_query("SELECT * FROM {pincode_login} WHERE ip = '%s'", $ip));
  return new pincode_login_fail($raw->ip, $raw->count, $raw->time);
}

/**
 * Checks if a given Ip adress is allowed to attempt login
 * 
 * @param   
 *    string, Ip adress of client
 *    
 * @return
 *    boolean
 * 
 */      
function _pincode_login_is_allowed_for_ip($ip) {
  if (!_pincode_login_ip_have_failed_before($ip)) {
    return TRUE;
  }
  else {
  
    $fail =  _pincode_login_get_previous_fail_for_ip($ip);
  
    return ($fail->fail_count_ok() || $fail->time_since_last_fail_ok());
  }
}

/**
 * Resets (deletes from db) records of failed logins 
 * from client
 * 
 * @param
 *    string, Ip adress of Client
 *    
 */      
function _pincode_reset_failed_logins($ip) {
  if (_pincode_login_ip_have_failed_before($ip)) {
    db_query("DELETE FROM {pincode_login} WHERE ip = '%s'", $ip);
  }
}

/**
 * Worker function for pincode form validator
 * Performs the logging in of the user
 * 
 * @param
 *   integer, Pincode to log in
 *   
 * @return
 *   boolean, true if login succeeded. False if failed.
 */        
function _pincode_login_dologin($pincode) {
  $account = user_external_load($pincode);
  if (isset($account->uid)) {
    user_external_login($account);
    return TRUE;
  } 
  watchdog('pincode_login', 'User failed logging in. Pincode: %pincode (%IP)', 
            array('%pincode' => $pincode), WATCHDOG_WARNING);
  return FALSE;
}

/**
 * Submit handler for the pincode login form
 * Really don't have anything to do here
 * Perhaps a default redirect?
 */
function pincode_login_pincode_form_submit($form, &$form_state) {
  //Do I really need to do anything here?
  //Rules is supposed to do redirection!
}

/**
 * Generates a unique 4 digit pincode
 *
 * @return
 *   integer, 4 digit pincode
 */
function _pincode_login_generate_pincode() {
  $exisiting_pincodes = _pincode_login_get_existing_pincodes();
  $pincode = rand(1000, 9999);
  while (in_array($pincode, $exisiting_pincodes)) {
    $pincode = rand(1000, 9999);
  }
  return $pincode;
}

/**
 * Gets existing pincodes.
 * Pincodes are stored in the authmap table in the coloumn
 * 'authname'
 * 
 * @return
 *   array of integers, 4 digit pincodes
 */      
function _pincode_login_get_existing_pincodes() {
  $pincodes = array();
  $result = db_query("SELECT authname FROM {authmap} WHERE module = '%s'", 'pincode_login');
    while ($o = db_fetch_object($result)) {
      $pincodes[] = $o->authname;
    }
  return $pincodes; 
}

/**
 * Register a new user and associates it with a pincode.
 * Pincode login uses the drupal framework for external
 * authentication providers, and thus the pincodes are stored
 * and managed by drupal as external authentication identifiers
 * 
 * @param
 *   string, unique username to register
 * 
 * @param
 *   string, email adress of user to register
 * 
 * @return
 *   integer, 4 digit pincode for user
 *     
 */
function pincode_login_register($username, $mail) {
  $pincode = _pincode_login_generate_pincode();
  $existing_user = user_load(array('name' => $username));
  if (isset($existing_user->uid)) { 
    watchdog('pincode_login', 'User with username: %name existed, and will be deleted and replaced,', 
              array('%name' => $username), WATCHDOG_WARNING);
    user_delete(array(), $existing_user->uid);
  }
  // Register this new user.
    $userinfo = array(
      'name' => $username,
      'mail' => $mail,
      'pass' => user_password(),
      'init' => $username,
      'status' => 1,
      'authname_pincode_login' => $pincode,
      'access' => time()
    );
    $account = user_save('', $userinfo);
    if (!$account) {
      watchdog('pincode_login', 'Failed registering new user (account save): %name', 
                array('%name' => $username), WATCHDOG_WARNING);
      return FALSE;
    }
    watchdog('pincode_login', 'New user registered: (account save): %name, %pincode', 
              array('%name' => $username, '%pincode' => $pincode), WATCHDOG_INFO);
  
  return array('pincodeuser' => $account, 'pincode' => $pincode);
}


/**
 * Implementation of hook_block().
 */
function pincode_login_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks[0] = array(
        'info' => t('Pincode login block')
      );
      return $blocks;
    
    case 'view':                
      if ($delta == 0) {
        $block = array(
          'subject' => t('Pincode Login'),
          'content' => drupal_get_form('pincode_login_pincode_form')
        );
      }
    return $block;
  } 
}