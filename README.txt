
Pincode Login module provides fascilities log in using only a 4 digit pin code 


- Due to the nature of this module, you need to create some logic surrounding creation 
of users and distribution of pincodes that fits your case. 
This can be done using the rules module (http://drupal.org/rules), as the pincode_login
module exposes it's functionality to this module.
It is allso possible to use the pincode_login_register($username, $mail) function in code.
- The pincode login module provides a block and a page to perform the login.
- Login attempts are limited to 3 attempts pr IP adress, then 1 attempt pr 15 minutes.

*SECURITY NOTE*
This module uses Drupals external login API to BYPASS drupals login security!!
This module is *LESS* secure than Drupals standard login.
Knowing this, having login by only a 4 digit pincode has it's uses. I.e. for short lived resources.

Installation
------------

Copy the module files to your module directory and then enable on the admin
modules page.

Author
------
Vidar Løvbrekke Sømme
vidar.l.somme@gmail.com