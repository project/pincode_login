<?php

/*
 * @file
 * This file contains the class definition for 
 * the pincode_login_fail class.
 * This class describes a failed login attempt
 */

class pincode_login_fail {
  //Ip adress of client that failed login
  public $ip;
  
  //number of failed login attempts for this ip
  public $count;
  
  //timestamp of last failed login attempt
  public $time;
  
  function __construct($ip, $count = 1, $time = NULL) {
    if (is_null($time)) $time = time();
    
    $this->ip = $ip;
    $this->count = $count;
    $this->time = $time;
  }
  
  /**
   * Checks if number of failed login attempts is 
   * below treshold value
   *  
   * @return
   *    boolean          
   */        
  function fail_count_ok() {
    return $this->count < 3;
  }
  
  /**
   * Checks if time since last failed login attempt is
   * below treshold value
   * 
   * @return
   *    boolean
   */
  function time_since_last_fail_ok() {
    return ($this->time + (15 * 60)) < time();
  }
  
  /**
   * Calculates number of minutes until next
   * login attempt is allowed
   * 
   * @return
   *    integer, number of minutes until next allowed login attempt
   */
  function time_until_next_allowed_attempt() {
    return ceil((($this->time + (15 * 60)) - time()) / 60);
  }
  
}