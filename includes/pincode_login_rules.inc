<?php

/**
 * @file
 * This file provides integration with the Rules module
 */ 

/**
 * Get a user object associated with a given pincode
 * Used by rules integration
 * 
 * @param
 *   integer, 4 digit pincode of user to be loaded
 *   
 * @return
 *   user, populated user object if succeeded. Nothing returned on fail.
 */         
function pincode_login_load_user_from_pincode($pincode) {
  $account = user_external_load($pincode);
  if (!isset($account->uid)) {
    watchdog('pincode_login', 'Failed loading user from pincode: %pincode', array('%pincode' => $pincode), WATCHDOG_ERROR);
    return FALSE;
  }
  return array('pincodeuser' => $account);
}

/**
 * Get a user object associated with a given username
 * Used by rules integration
 * 
 * @param
 *   string, username to be loaded
 *   
 * @return
 *   user, populated user object if succeeded. FALSE returned on fail.
 */     
function pincode_login_load_user_from_username($username) {
  return array('pincodeuser' => user_load(array('name' => $username)));
}

/**
 * Get the pincode for a given user id
 * Used by rules integration
 * 
 * @param
 *   integer, user id
 *   
 * @return
 *   string, Pincode of given uid
 */     
function pincode_login_get_pincode_for_user($uid) {
  return array('pincode' => db_result(db_query_range("SELECT authname FROM {authmap} WHERE uid = %d AND module = 'pincode_login'", $uid, 0, 1)));
}

/**
 * Change the email adress og a given user
 * Used by rules integration
 * 
 * @param
 *   user, user to change adress of
 *   string, new email adress 
 *
 */     
function pincode_login_change_user_email($user, $newmail) {
  user_save($user, array('mail' => $newmail));
}

/**
 * Implementation of hook_rules_action_info().
 */ 
function pincode_login_rules_action_info() {
  return array(
    'pincode_login_register' => array(
      'label' => t('Generate a new user with pincode login'),
      'arguments' => array(
        'username' => array('type' => 'string', 'label' => t('Username of new user')),
        'email' => array('type' => 'string', 'label' => t('Email adress of new user')),
      ),
      'new variables' => array(
        'pincodeuser' => array(
          'type' => 'user',
          'label' => t('Newly generated user')
        ),
        'pincode' => array(
          'type' => 'string',
          'label' => t('Pincode of newly created user')
        )
      ),
      'module' => 'pincode_login'
    ),
    
    'pincode_login_load_user_from_pincode' => array(
      'label' => t('Load a user object from pincode'),
      'arguments' => array(
        'pincode' => array('type' => 'string', 'label' => t('Pincode of user to load'))
      ),
      'new variables' => array(
        'pincodeuser' => array(
          'type' => 'user',
          'label' => t('User loaded from pincode')
        )
      ),
      'module' => 'pincode_login'
    ),
    
    'pincode_login_load_user_from_username' => array(
      'label' => t('Load a user object from username'),
      'arguments' => array(
        'username' => array('type' => 'string', 'label' => t('Username of user to load'))
      ),
      'new variables' => array(
        'pincodeuser' => array(
          'type' => 'user',
          'label' => t('User loaded from username')
        )
      ),
      'module' => 'pincode_login'
    ),
    
    'pincode_login_get_pincode_for_user' => array(
      'label' => t('Get pincode from User by user ID'),
      'arguments' => array(
        'username' => array('type' => 'number', 'label' => t('User id to get pincode for'))
      ),
      'new variables' => array(
        'pincode' => array(
          'type' => 'string',
          'label' => t('Pincode for user')
        )
      ),
      'module' => 'pincode_login'
    ),
    
    'pincode_login_change_user_email' => array(
      'label' => t('Change email adress of a user'),
      'arguments' => array(
        'user' => array('type' => 'user', 'label' => t('User to change email adress of')),
        'mail' => array('type' => 'string', 'label' => t('New email adress'))
      ),
      'module' => 'pincode_login'
    ),
    
  );
}